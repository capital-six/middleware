﻿using System;
using System.Linq;
using System.Net;
using CapitalSix.Attributes;
using CapitalSix.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Logging;

namespace CapitalSix.Middleware.JsonExceptions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseExceptionHandler (this IApplicationBuilder applicationBuilder)
        {
            return applicationBuilder.Use (async (context, next) =>
            {
                try
                {
                    await next.Invoke ();
                }
                catch (Exception ex)
                {
                    var statusCode = HttpStatusCode.InternalServerError; // 500 if unexpected
                    var intlId = null as string;

                    var exceptionType = ex.GetType ();
                    var httpStatusAttribute = exceptionType
                        .GetCustomAttributes (true)
                        .SingleOrDefault (a => a.GetType () == typeof (HttpStatusAttribute)) as HttpStatusAttribute;

                    if (httpStatusAttribute != null)
                    {
                        statusCode = httpStatusAttribute.Status;
                        intlId = httpStatusAttribute.IntlId;
                    }
                    else if (ex is UnauthorizedAccessException)
                        statusCode = HttpStatusCode.Unauthorized;

                    var result = new { Error = ex.Message, IntlId = intlId };
                    await context.Response.WriteJsonAsync (result, statusCode);
                }
            });
        }
    }
}