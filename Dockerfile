FROM mcr.microsoft.com/dotnet/core/sdk:3.0 AS build

RUN apt-get install --yes curl
RUN curl --silent --location https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install --yes nodejs

WORKDIR /app
COPY . .
COPY nuget.config.source nuget.config

ARG NUGET_REPOSITORY
ARG NUGET_APIKEY
ARG PACKAGE_VERSION

#RUN dotnet restore
RUN dotnet restore

# copy everything else and build app
RUN dotnet pack \
  --no-restore \
  --configuration Release \
  --output /app/out \
  -p:PackageVersion=$PACKAGE_VERSION \
  -p:RepositoryBranch=${GIT_COMMIT_BRANCH} \
  -p:RepositoryCommit=${GIT_COMMIT_SHA} \
  -p:RepositoryUrl=${GIT_PROJECT_URL}

WORKDIR /app/out
RUN dotnet nuget push --api-key=${NUGET_APIKEY} --source=${NUGET_REPOSITORY} ./CapitalSix.Middleware.${PACKAGE_VERSION}.nupkg
